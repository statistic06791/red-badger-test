import { assertEquals, assertThrows } from 'std/testing/asserts.ts';
import {parseInput, Size, size} from './input.ts';

Deno.test('size can parse some examples', () => {
	const {maxX: x1, maxY: y1} = size.parse('5 3');
	assertEquals(x1, 5);
	assertEquals(y1, 3);

	const {maxX: x2, maxY: y2} = size.parse('1 50');
	assertEquals(x2, 1);
	assertEquals(y2, 50);
});

Deno.test('size rejects alphabets', () => {
	assertThrows(() => size.parse('5 A'));
	assertThrows(() => size.parse('C 20'));
});

Deno.test('can parse test input', async () => {
	const contents = await Deno.readTextFile('src/testdata/input_1.txt');
	const gameInput = parseInput(contents);
	assertEquals(gameInput.boardSize, {maxX: 5, maxY: 3} satisfies Size);
	assertEquals(gameInput.robotInputs.length, 3);

	const input1 = gameInput.robotInputs[0];
	assertEquals(input1.startPosition.x, 1);
	assertEquals(input1.startPosition.y, 1);
	assertEquals(input1.startPosition.orientation, 'E');
	assertEquals(input1.instructions.length, 8);

	const input2 = gameInput.robotInputs[1];
	assertEquals(input2.instructions.length, 13);

	const input3 = gameInput.robotInputs[2];
	assertEquals(input3.instructions.length, 10);
});
