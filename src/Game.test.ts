import { parseInput } from "./input.ts";
import { assert, assertEquals } from "std/testing/asserts.ts";
import { Game } from "./Game.ts";

Deno.test("Plays provided test input", async () => {
  const contents = await Deno.readTextFile("src/testdata/input_1.txt");
  const gameInput = parseInput(contents);
  const game = new Game(gameInput);
  game.play();
  const output = game.output;

  assertEquals(output.length, 3);
  assertEquals(output[0], "1 1 E");
  assertEquals(output[1], "3 3 N LOST");
  assertEquals(output[2], "2 3 S");

  const expectedOutput = await Deno.readTextFile(
    "src/testdata/expected_output_1.txt"
  );
  assertEquals(output.join("\n") + "\n", expectedOutput);
});
