# Red Badger Test

Using Typescript and [Deno](https://deno.land/). I've never used Deno before and wanted to try it out. Also, you might notice the use of union types and [discriminated unions](https://www.typescriptlang.org/docs/handbook/2/narrowing.html#discriminated-unions) - they look like magic strings but they're *type safe*.

## Things I learnt
- Deno basics
- Practiced more zod transforms

## Usage
- Install [deno](https://deno.land/manual@v1.35.1/getting_started/installation#download-and-install)
- Run task: `deno task $task_name`, e.g. `deno task sampleInput`
  - Some test files also need an extra `--allow-read`
- Run all tests: `deno task test`
  - [Deno.test documentation](https://deno.land/api@v1.35.1?s=Deno.test)
  - [Deno asserts documentation](https://deno.land/std@0.194.0/testing/asserts.ts)
- List possible tasks: `deno task` or read `deno.json`
- When adding a dependency to a file, you can download them by running: `deno cache src/main.ts`

## Input format
- First line (top right max width and height)
- after that, pairs of:
  - starting coordinates + orientation
  - move instructions

## Interesting thoughts about Deno
- Typescript and build configuration is easy/minimal.
- Uses json or jsonc (json with comments) for configuration, instead of yaml.
- A weird/bad ways of [importing dependencies](https://deno.land/manual@v1.35.1/examples/manage_dependencies): `deps.ts` imports all the remote dependencies, so all your other files just imports from `deps`. Alternatively, you can define import map in `deno.json`.
- It's nice I don't have to pick test libraries to do unit tests (e.g. between vitest, jest)
- Deno test APIs are mixed between `std/testing` and the `Deno` global variable. Autoimports are not very good.
- I probably won't suggest it in a bigger project.
