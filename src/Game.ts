import { GameInput, RobotInput, RobotPosition } from './input.ts';

// What happens when a robot blocks the path? Assume multiple robots can share a cell.
export class Game {
  // the string is formatted: `x y orientation`, e.g. `5 5 S` or `23 12 N`
	private scents = new Set<string>();

	constructor(private readonly input: GameInput) {}

	private processRobotInput = (input: RobotInput) => {
		console.log("\nnew robot")
		const currentPosition = { ...input.startPosition };

		let lost = false;
		processInstructions:
		for (const instruction of input.instructions) {
			console.log(JSON.stringify(currentPosition), instruction)
			switch (instruction) {
				case 'F':
					lost = this.moveForward(currentPosition)
					if (lost) {
						break processInstructions;
					}
					break;
				case 'L':
					this.lookLeft(currentPosition);
					break;
				case 'R':
					this.lookRight(currentPosition);
					break;
			}
		}

		console.log(JSON.stringify(currentPosition))

		// If this application output was useful, I would use a type instead of a string.
		const { x, y, orientation } = currentPosition;
		const lostSuffix = (lost) ? " LOST" : "";
		this.output.push(`${x} ${y} ${orientation}${lostSuffix}`);
	};

	private moveForward = (position: RobotPosition): boolean => {
		const startX = position.x;
		const startY = position.y;

		switch (position.orientation) {
			case 'N':
        if (!this.scents.has(`${position.x} ${position.y + 1} ${position.orientation}`)) position.y += 1;
				break;
			case 'E':
        if (!this.scents.has(`${position.x + 1} ${position.y} ${position.orientation}`)) position.x += 1;
				break;
			case 'W':
        if (!this.scents.has(`${position.x - 1} ${position.y} ${position.orientation}`)) position.x -= 1;
				break;
			case 'S':
        if (!this.scents.has(`${position.x} ${position.y - 1} ${position.orientation}`)) position.y -= 1;
				break;
		}

		const xOutOfBounds = position.x < 0 ||
			position.x > this.input.boardSize.maxX;
		const yOutOfBounds = position.y < 0 ||
			position.y > this.input.boardSize.maxY;
		if (xOutOfBounds || yOutOfBounds) {
			this.scents.add(`${position.x} ${position.y} ${position.orientation}`);
			position.x = startX;
			position.y = startY;
			return true;
		}

		return false;
	};

	private lookLeft = (position: RobotPosition) => {
		/**
		 *    N
		 *  W   E
		 *    S
		 */
		switch (position.orientation) {
			case 'N':
				position.orientation = 'W';
				break;
			case 'E':
				position.orientation = 'N';
				break;
			case 'W':
				position.orientation = 'S';
				break;
			case 'S':
				position.orientation = 'E';
				break;
		}
	};

	private lookRight = (position: RobotPosition) => {
		/**
		 *    N
		 *  W   E
		 *    S
		 */
		switch (position.orientation) {
			case 'N':
				position.orientation = 'E';
				break;
			case 'E':
				position.orientation = 'S';
				break;
			case 'W':
				position.orientation = 'N';
				break;
			case 'S':
				position.orientation = 'W';
				break;
		}
	};

	private _output = [];
	public get output(): string[] {
		return this._output;
	}

	play() {
		for (const input of this.input.robotInputs) {
			this.processRobotInput(input);
		}
	}
}
