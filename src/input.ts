import {z} from 'https://deno.land/x/zod@v3.20.2/mod.ts';

export const orientation = z.union([
	z.literal('N'),
	z.literal('E'),
	z.literal('W'),
	z.literal('S'),
]);
export type RobotOrientation = z.infer<typeof orientation>;

// Zod used to validate inputs because it is more declarative and composable.
// I get the typescript type and validator by writing the zod type.
// For such a small application this effort might not have been worth it.

export const size = z
	.string()
	.transform((val, ctx) => {
		const tokens = val.split(' ');
		if (tokens.length !== 2) {
			ctx.addIssue({
				code: z.ZodIssueCode.custom,
				message:
					'Input must contain exactly 2 numbers separated by spaces',
			});
			return z.NEVER;
		}
		const [x, y] = [parseInt(tokens[0]), parseInt(tokens[1])];
		if (isNaN(x) || isNaN(y)) {
			ctx.addIssue({
				code: z.ZodIssueCode.custom,
				message: `Not a number. x: ${x}. y: ${y}.`,
			});
			return z.NEVER;
		}
		return { maxX: x, maxY: y };
	});
export type Size = z.infer<typeof size>;

export const robotPosition = z
	.string()
	.transform((val, ctx) => {
		const tokens = val.split(' ');
		if (tokens.length !== 3) {
			ctx.addIssue({
				code: z.ZodIssueCode.custom,
				message:
					'Input must contain exactly 2 numbers and 1 character separated by spaces',
			});
			return z.NEVER;
		}
		const x = parseInt(tokens[0]);
		const y = parseInt(tokens[1]);
		const o = orientation.parse(tokens[2]);
		if (isNaN(x) || isNaN(y)) {
			ctx.addIssue({
				code: z.ZodIssueCode.custom,
				message: `Not a number. x: ${x}. y: ${y}.`,
			});
			return z.NEVER;
		}
		return { x, y, orientation: o };
	});
export type RobotPosition = z.infer<typeof robotPosition>;

export const robotInstruction = z.union([
	z.literal('L'),
	z.literal('R'),
	z.literal('F'),
]);
export type RobotInstruction = z.infer<typeof robotInstruction>;
const robotInstructions = z
	.string()
	.transform((val, _ctx) =>
		val.split('').map((i) => robotInstruction.parse(i))
	);

export const robotInput = z.object({
	startPosition: robotPosition,
	instructions: robotInstruction.array(),
});
export type RobotInput = z.infer<typeof robotInput>;

// Aka test input
export type GameInput = {
	boardSize: Size;
	robotInputs: RobotInput[];
};

export const parseInput = (input: string): GameInput => {
	const lines = input.split('\n');
	const boardSize = size.parse(lines[0]);

	const robotInputs: RobotInput[] = [];
	let tempStartPosition: RobotPosition | undefined = undefined;
	for (const line of lines.slice(1)) {
		const trimmedLine = line.trim();
		if (trimmedLine.length === 0) continue;

		if (!tempStartPosition) {
			tempStartPosition = robotPosition.parse(trimmedLine);
		} else {
			robotInputs.push({
				startPosition: tempStartPosition,
				instructions: robotInstructions.parse(trimmedLine),
			});
			tempStartPosition = undefined;
		}
	}

	return {
		boardSize,
		robotInputs,
	};
};
