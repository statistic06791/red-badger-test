import { Game } from './Game.ts';
import { parseInput } from './input.ts';

const inputFilepaths = Deno.args;

if (inputFilepaths.length !== 1) {
	throw new Error(
		`Expected 1 position argument for input file path, but received ${inputFilepaths.length}`,
	);
}

const contents = await Deno.readTextFile(inputFilepaths[0]);
const gameInput = parseInput(contents);

const game = new Game(gameInput);
game.play();
const output = game.output;

const encoder = new TextEncoder();
const buffer = encoder.encode(output.join('\n') + '\n');
Deno.stdout.write(buffer);
